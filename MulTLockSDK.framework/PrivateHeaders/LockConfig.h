//
//  LockConfig.h
//  MultiLock
//
//  Created by Eldad Ohana on 12/25/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//

#import <Foundation/Foundation.h>



//For NIZ locker
///buzzer volume = max - for NIZ locker
static const Byte BUZZER_VOLUME_MAX = 0;
///buzzer volume = med hight - for NIZ locker
static const Byte BUZZER_VOLUME_MEDH = 1;
///buzzer volume = med low - for NIZ locker
static const Byte BUZZER_VOLUME_MEDL = 4;
///buzzer volume = mute - for NIZ locker
static const Byte BUZZER_VOLUME_MUTE = 5;
///buzzer volume = high volume musk = 11111010 - for NIZ locker
static const Byte BUZZER_VOLUME_HIGH_VOLUME_MASK = 250;
///buzzer volume = mid high volume musk = 11111011 - for NIZ locker
static const Byte BUZZER_VOLUME_MID_HIGH_VOLUME_MASK = 251;
///buzzer volume = mid low volume musk = 11111110 - for NIZ locker
static const Byte BUZZER_VOLUME_MID_LOW_VOLUME_MASK = 254;

///defines ENTR locker volume
typedef enum {
    ENTR_VOLUME_ON = 1,
    ENTR_VOLUME_OFF = 0,
} EntrLockerVolume;


///defines locker volume
typedef enum {
    MAX_VOLUME = 3,
    MID_HIGH_VOLUME = 2,
    MID_LOW_VOLUME = 1,
    MUTE_VOLUME = 0
} LockerVolume;

///ENTR buzzer volume MUTE = 11111011
static const Byte ENTER_VOLUME_MUTE_MASK = 251;
///ENTR buzzer volume ON = 00000100
static const Byte ENTER_VOLUME_ON_MASK = 5;



@interface LockConfig : NSObject

@property (nonatomic, strong) NSString *ownerCode;
@property (nonatomic, strong) NSString *theNewCode;


///Defines the volume level for locker
@property (nonatomic, assign) LockerVolume Volume;
//@property (nonatomic, assign) BOOL mute;

@property (nonatomic, assign) BOOL autoLock;
@property (nonatomic, assign) BOOL allowRemote;
@property (nonatomic, strong) NSString *requiresCode;
@property (nonatomic, strong) NSString *lockName;




@end
