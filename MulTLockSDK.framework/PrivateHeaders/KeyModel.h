//
//  KeyModel.h
//  MultiLock
//
//  Created by Eldad Ohana on 10/7/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface KeyModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *name;
@property (nonatomic ,strong) NSString *uniqueIdOflock;
@property (nonatomic, strong) NSData *keyData;
@property (nonatomic, strong) NSString *keyPassword;
@property (nonatomic, strong) UserModel *user;
@property (nonatomic, assign) int expireInHours;

@end
