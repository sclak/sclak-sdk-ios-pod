//
//  AppCodeModel.h
//  MultiLock
//
//  Created by Eldad Ohana on 12/23/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppCodeModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) BOOL codeEnable;
@property (nonatomic, strong) NSString *currentQuestion;
@property (nonatomic, strong) NSString *currentAnswer;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) BOOL didAskForPasswordThisSession;

@end
