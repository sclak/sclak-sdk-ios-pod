//
//  LockDevice.h
//  MultiLock
//
//  Created by Eldad Ohana on 8/27/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//
//  LockModel is an abstract model, please do not use it directly, please use MLLockDevice.
//  The use of object methods and properties getters/setters are not supported and in future may be deprecated.   


#import "LockConfig.h"
#import <Foundation/Foundation.h>

static const Byte AUTO_BIT_VALUE = 2;
static const Byte MUTE_BIT_VALUE = 4;
static const Byte LOCK_BIT_VALUE = 8;
static const Byte DOOR_BIT_VALUE = 16;
static const Byte CHARGE_BIT_VALUE = 32;
static const Byte BATTERY_BIT_VALUE_LOW = 64;
static const Byte BATTERY_BIT_VALUE_HIGH = -128;

static const int BATTERY_STATE_LOW = 0;
static const int BATTERY_STATE_MEDIUM = 1;
static const int BATTERY_STATE_HIGH = 2;

static const int BATTERY_MED_FULL_LIMIT = 20;
static const int BATTERY_LOW_MED_LIMIT = 10;

static const Byte LOCK_STATE_UNINITIALIZED = 0;
static const Byte LOCK_STATE_INITIALIZED = 1;
static const Byte LOCK_STATE_INITIALIZED_WITH_KEYS_PENDING = 2;
static const Byte LOCK_STATE_INITIALIZED_WITH_FAST_KEYS_PENDING = 3;
static const Byte LOCK_STATE_INITIALIZED_AND_NOT_CALIBRATED = 4;

static NSString *LOCK_VERSION_1_27_R_7 = @"1.27r7"; // 88
static NSString *LOCK_VERSION_1_28_R_1 = @"1.28r1"; // 142
static NSString *LOCK_VERSION_1_28_R_6 = @"1.28r6"; // 168
static NSString *LOCK_VERSION_1_29_R_1 = @"1.29r1"; // 198

///value for battery percentage for new lock's
static const int DEFAULT_BATTERY_PERCENTAGE_4_NEW_LOCKS = -1;


///defines additional device status
typedef enum  {
    NOT_EXIST = 0,
    DISABLED = 1,
    ACTIVE = 2,
    REQUESTED = 3,
    PENDING = 4
} AdditionalDeviceStatus;

///defines additional device status
typedef enum   {
    DISABLED_KEY = 0,
    ENABLED_KEY = 1,
    PENDING_KEY = 2
} AdditionalDeviceStatusKeys;

///defines the door direction
typedef  enum {
    LEFT,
    RIGHT
} DoorDirection;

//sdk
///defines the lock type
typedef  enum {
    NORMAL,
    HANDLE_LIFT
} LockerType;

@interface LockDevice : NSObject <NSCopying>

- (instancetype) initWithClass:(NSString *)className;

@property (nonatomic, assign) Byte lockState;
@property (nonatomic, assign) Byte lockAdvertisingVersion;
@property (nonatomic, assign) Byte deviceStatus;

///Value used for old lock's prior to NIZ, for NIZ or newer lock's the value will be -1
@property (nonatomic, assign) NSInteger batteryPercent;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *communicationVersion;
@property (nonatomic, strong) NSString *codeVersion;
@property (nonatomic, strong) NSDate *connectionTime;

/// The id for the lock in Core Bluetooth system.
@property (nonatomic, strong) NSUUID *deviceID;

/*!
 * Indicates if the lock is a NIZ model locker.
 */
@property (atomic, assign) BOOL NIZLocker;
/// indicates if requires code to unlock the locker
@property (atomic, assign) BOOL requiresCode2Unlock;
/// indicates the status of wall reader device
@property (atomic, assign) AdditionalDeviceStatus wallReaderStatus;
/// indicates the status of the integration unit device
@property (atomic, assign) AdditionalDeviceStatus integrationUnitStatus;

/// indicates the door direction
@property (atomic, assign) DoorDirection doorDirection;
/// indicates the locker type
@property (atomic, assign) LockerType lockerType;

- (int)GetBatteryState;
- (BOOL)IsLockAutoLocked;
- (BOOL)IsLockLocked;
- (BOOL)IsDoorClosed;
- (BOOL)IsLockCharging;
- (BOOL)IsLockMuted;

-(void)setLockToUnMute;
-(void)setLockToMute;

//for NIZ locker
-(void)setLockToMaxVolume;
-(void)setLockToMedHighVolume;
-(void)setLockToMedLowVolume;

-(LockerVolume)getLockerVolume;
-(void)assignWallReaderStatus: (Byte) byte;
-(void)assignIntegrationUnitStatus: (Byte) byte;
-(void)assignDoorDirection: (Byte) byte;
-(void)assignLockerType: (Byte) byte;

-(void)setLockToManule;
-(void)setLockToAuto;

@end
