//
//  UserModel.h
//  MultiLock
//
//  Created by Eldad Ohana on 10/7/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, assign) int role;
@property (nonatomic, strong) NSString *password;

@end
