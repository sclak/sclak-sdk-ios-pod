//
//  AppModel.h
//  MultiLock
//
//  Created by Eldad Ohana on 10/7/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppCodeModel.h"
#import "MLLockModel.h"

@interface AppModel : NSObject <NSCoding>

@property (nonatomic, strong) NSString *appID;
@property (nonatomic, strong) NSArray *locks;
@property (nonatomic, strong) AppCodeModel *codeModel;

- (void)SaveModel;

@end
