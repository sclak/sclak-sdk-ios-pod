//
//  LockModel.h
//  MultiLock
//
//  Created by Eldad Ohana on 8/31/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//
//  LockModel is an abstract model, please do not use it directly, please use MLLockModel.
//  The use of object methods and properties getters/setters are not supported and in future may be deprecated.


#import <Foundation/Foundation.h>
#import "KeyModel.h"
#import "UserModel.h"
#import "MLLockDevice.h"
#import "AppCodeModel.h"


#define LOCK_MODEL_ZONE_NA ((int) 1)
@interface LockModel : NSObject <NSCoding>

- (instancetype) initWithClass:(NSString *)className;

@property (nonatomic, strong) LockDevice *device;
@property (nonatomic, strong) KeyModel *key;
@property (nonatomic, strong) NSString *name;
//the new naming system from oct 2015
-(BOOL)doesLockSupportNewNaming;

@property (nonatomic, strong) NSString *lockSN;
@property (nonatomic, strong) AppCodeModel *codeModel;
@property (nonatomic, strong) NSString *deviceID;
@property (nonatomic, assign) int mode;
/*! brand id for the lock */
@property (nonatomic, assign) NSInteger providerID;
@property (nonatomic, assign) NSNumber* isStagingServer;

/*! The app id in the lock's array */
@property (nonatomic, assign) int kdfID;
/*! The generated key from private and public */
@property (nonatomic, strong) NSData *sharedKey;
@property (nonatomic, strong) NSString *uniqeueSKey;
@property (nonatomic, strong) NSData *currentIV;


/// The unique id for the lock.
@property (nonatomic, strong) NSString *lockUniqeId;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *updateStatus;
@property (nonatomic, strong) NSNumber *modelZone;
@property ( atomic, assign) BOOL NIZLocker;

-(LockDevice *)getLockDevice;
-(KeyModel *)getLockKey;
-(NSString *)getLockName;
-(NSString *)getLockKeyName;
-(NSInteger)getUserRole;
-(NSString *)getLockUniqueID;
-(NSString *)getLockModel;


@end
