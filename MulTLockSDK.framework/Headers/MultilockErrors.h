//
//  MultilockErrors.h
//  Mul-T-LockSDK
//
//  Created by Asher Harel on 06/11/2016.
//  Copyright © 2016 Asher Harel. All rights reserved.
//
//  MultilockErrors class features the NSError Attributes (domain, code and description) received in Multilock SDK request callbacks.
//  Upon Multilock SDK error request, NSError will be implemented and return in one of the following:
//  1. request method callback.
//  2. MultilockDelegate delegate methods.
//
//  Error object attributes as listed below:
//  1. MLBleErrorDomain - iPhone device BLE low level errors. MLBleErrorCode (1 - 5) as listed in NS_ENUM below.
//  2. MLSDKErrorCode - Multilock SDK errors. MLSDKErrorCode (1 - 10).
//  3. MLLockErrorDomain - lock device errors. MLLockErrorCode (1 - 33, 102).
//  4. MLServerErrorDomain - server request errors. MLServerErrorCode (1).
//
//  Error description may exist in errors with key: MLErrorDescriptionKey

#import <Foundation/Foundation.h>

#define MLBleErrorDomain @"com.MultilockSDK.BLE.ErrorDomain";

typedef NS_ENUM(NSInteger, MLBleErrorCode) {
    BLEPowerError = 1,
    BLECharacteristicError = 2, //read write
    BLEServicesDiscoveredError = 3,
    BLEDisconnectError = 4,
    BLEConnectError = 5
};

#define MLSDKErrorDomain @"com.MultilockSDK.SDK.ErrorDomain";

typedef NS_ENUM(NSInteger, MLSDKErrorCode) {
    TimeOutError        = 1,
    PasswordError       = 2,
    PermissionError     = 3,
    NameError           = 4,
    LockConnectionError = 5,
    LockNameError       = 6,
    InputEmailError     = 7,
    PhoneNumberError    = 8,
    LockSNError         = 9,
    KeyError            = 10,
    storageError        = 11,
    LockNotFoundError   = 12
};


#define MLLockErrorDomain @"com.MultilockSDK.Lock.ErrorDomain";

typedef NS_ENUM(NSInteger, MLLockErrorCode) {
    ERROR_DETAIL_WrongPublicKeyLength   = 1,
    ERROR_DETAIL_CantSendPublicKey      = 2,
    ERROR_DETAIL_UnsupportedCommand     = 3,
    ERROR_DETAIL_UnsupportedCmdInBuff   = 4,
    ERROR_DETAIL_WrongAppIDRandLength   = 5,
    ERROR_DETAIL_WrongSetOwnerLength    = 6,
    ERROR_DETAIL_WrongOwnerCode         = 7,
    ERROR_DETAIL_CantSendSetOwnerResp   = 8,
    ERROR_DETAIL_WrongKDFLength         = 9,
    ERROR_DETAIL_WrongCreateKeyLength   = 10,
    ERROR_DETAIL_CantSendCreateKeyResp  = 11,
    ERROR_DETAIL_WrongGetKeyLength      = 12,
    ERROR_DETAIL_WrongUserID            = 13,
    ERROR_DETAIL_WrongPIN               = 14,
    ERROR_DETAIL_KeyGenerationError     = 15,
    ERROR_DETAIL_WrongUnlockLength      = 16,
    ERROR_DETAIL_WrongEKey              = 17,
    ERROR_DETAIL_WrongConfigLength      = 18,
    ERROR_DETAIL_WrongCfgParamLength    = 19,
    ERROR_DETAIL_UnsupportedConfig      = 20,
    ERROR_DETAIL_CantSendConfigResp     = 21,
    ERROR_DETAIL_CantSendOpSuccess      = 22,
    ERROR_DETAIL_LockBusy               = 23,
    ERROR_DETAIL_LockMainCommError      = 24,
    ERROR_DETAIL_LockError              = 25,
    ERROR_DETAIL_LockInqEarlyTermination = 26,
    ERROR_DETAIL_LockTimeout            = 27,
    ERROR_DETAIL_CommandSuspended       = 28,
    ERROR_DETAIL_WrongUnlockCode        = 29,
    ERROR_DETAIL_FOTA_Insufficient_DP_size = 30,
    ERROR_DETAIL_KeysLimitReached       = 31,
    ERROR_DETAIL_InvalidLockLength      = 32,
    ERROR_DETAIL_BatteryLow     = 33,
    CMD_OP_ERROR    = 102
};

#define MLServerErrorDomain @"com.MultilockSDK.Server.ErrorDomain";

typedef NS_ENUM(NSInteger, MLServerErrorCode) {
    serverBrandError = 1
};

#define MLErrorDescriptionKey @"errorDescription";

