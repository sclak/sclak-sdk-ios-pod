//
//  MLLockDevice.h
//  Mul-T-LockSDK
//
//  Created by Asher Harel on 19/11/2016.
//  Copyright © 2016 Asher Harel. All rights reserved.
//
//  MLLockDevice object inherited from abstract LockDevice, and represents lock status and configuration.

#import "LockDevice.h"

@interface MLLockDevice : LockDevice
/// indicates the door direction
@property (atomic, assign) DoorDirection doorDirection;
/// indicates the locker type
@property (atomic, assign) LockerType lockerType;

- (int)GetBatteryState;

- (BOOL)IsLockAutoLocked;

- (BOOL)IsLockLocked;

- (BOOL)IsDoorClosed;

- (BOOL)IsLockCharging;

- (BOOL)IsLockMuted;

@end
