//
//  MLLockModel.h
//  Mul-T-LockSDK
//
//  Created by Asher Harel on 19/11/2016.
//  Copyright © 2016 Asher Harel. All rights reserved.
//
//  MLLockModel object inherited from abstract LockModel, and represents lock attributes (getters), and includes LockDevice and KeyModel objects.

#import "LockModel.h"

@interface MLLockModel : LockModel

-(LockDevice *)getLockDevice;

-(KeyModel *)getLockKey;

-(NSString *)getLockName;

-(NSString *)getLockKeyName;

-(NSInteger)getUserRole;

-(NSString *)getLockUniqueID;

-(NSString *)getLockModel;

@end
