//
//  UserInLock.h
//  MultiLock
//
//  Created by Eldad Ohana on 11/26/14.
//  Copyright (c) 2014 mobixon. All rights reserved.
//
//  Object returns from lock on request: MLGetUsersFromLockWithPassword  


#import <Foundation/Foundation.h>

static const NSInteger ENUM_ROLE_USER = 0;
static const NSInteger ENUM_ROLE_ADMIN = 1;
static const NSInteger ENUM_ROLE_OWNER = 2;


@interface UserInLock : NSObject <NSCopying>

@property (nonatomic, strong) NSString *username;
@property (nonatomic, assign) int role;
@property (nonatomic, assign) int keyState;

@end
