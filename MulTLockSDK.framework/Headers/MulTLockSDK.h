//
//  MulTLockSDK.h
//  Mul-T-LockSDK
//
//  Created by Asher Harel on 27/10/2016.
//  Copyright © 2016 Asher Harel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLLockModel.h"


@protocol MultilockDelegate <NSObject>

/**
 * MLFoundNewlocks delegate gets called as response to MLScanForLocks request. 
 * The new locks found after iPhone BLE scan are returned as objects in NSArray. 
 * @param locks - NSArray of objects with type LockDevice. LockDevice represent locks that were found during single BLE scan.
 */
-(void)MLFoundLocks:(nullable NSArray *)locks;

/**
 * MLOnLockConnection delegate gets called as response to MLConnectToLock request.
 * @param success - BOOL represents connection request result.
 * @param lockID - NSUUID of lock requested to connect.
 * @param error - NSError request.
 */
-(void)MLOnLockConnection:(BOOL)success lockID:(nullable NSUUID *)lockID withError:(nullable NSError *)error;

/**
 * MLDisconnectLockID delegate gets called as response to MLDisconnectLock.
 * @param lockID - NSUUID of lock requested to connect.
 * @param error - NSError request.
 */
-(void)MLDisconnectLockID:(nullable NSUUID *)lockID withError:(nullable NSError *)error;


/**
 * MLOnErrorCallback delegate gets called when unexpected errors occur while using Multilock SDK.
 * For More information Please see MultilockErrors class.
 * @param error - NSError representing error domain, code and description.
 */
-(void)MLOnErrorCallback:(nonnull NSError *)error;



@end


/**
 * MulTLockSDK - header file, representing Multilock SDK interface.
 */
@interface MulTLockSDK : NSObject

///MulTLockSDK class shared instance;
+(nonnull MulTLockSDK *)MLSharedInstance;

@property (nonatomic, weak) _Nullable id<MultilockDelegate> MLDelegate;


/**
 * MLScanForLocks scan for all lock devices in the area. The scan duration is 2.4 seconds.
 */
-(void)MLScanForLocks;

/**
 * MLIsScanning - is method MLScanForLocks is being implemented now, and BLE is currently scanning for locks.
 * @return - BOOL value True/false;
 */
-(BOOL)MLIsScanning;


/**
 * MLListOfScanedLocks returns The list of last scanned locks.
 * The locks added to list, are available locks (not connected to any smart phone device at the moment) that transmit one of the following status: Uninitialize, initialize or initialize+have pending keys.
 * The locks in list are divided to 4 separate locks arrays, according to status transmitted:MLUninitializedLocks, MLInitializedLocks, MLInitializedWithPendingKeyLocks, MLPairedLocks.
 * @return NSArray of scanned object of type (LockDevice *). Object represent Lock Device.
 */
-(nullable NSArray *)MLListOfScanedLocks;

///List of locks saved in app. These locks were used in app and their data is saved in app cache and storage memory.
@property (nullable, strong, readonly)NSArray * MLAppLocks;

///List of locks waiting for initialisation. The array contains objects of type LockModel. LockModel represent lock attributes, device, and key.
@property (nullable, strong, readonly)NSArray * MLUninitializedLocks;

///List of initialized locks. Locks in list are available for use.
@property (nullable, strong, readonly)NSArray *MLInitializedLocks;

///List of initialized locks with key pending. Locks in list available for use and have keys pending. pending key can be collected and use.
@property (nullable, strong, readonly)NSArray *MLInitializedWithPendingKeyLocks;

///List of initialized locks that current user don't have keys for. Such locks can be recovered using: MLRecoverOwnerLockWithPassword method.
@property (nullable, strong, readonly)NSArray *MLPairedLocks;



/**
 * MLConnectToLock - request to connect to lock.
 * @param lockModel - representing specific lock for connection.
 * Please note, only one user can be connected to lock at any time.
 * Please see MLOnLockConnection delegate in MultilockDelegate protocol for response.
 */
- (void)MLConnectToLock:(nonnull LockModel *)lockModel;


/**
 * MLDisconnectLock - request to disconnect current connected lock.
 * Please see MLDisconnectLockID delegate in MultilockDelegate protocol for response.
 */
-(void)MLDisconnectLock;


/**
 * MLGetConnectedLock returns LockModel object, representing current connected Lock.
 * @return - (LockModel*) connected lock.
 */
-(nullable LockModel *)MLGetConnectedLock;


/**
 * MLSetOwnerLockWithPassword - method sets user as lock (current connected lock) owner.
 * The lock have a single owner, when set owner request succeed, the owner can unlock/lock lock device, set lock settings and add, enable/disable users (keys) to lock.
 * @param password - 6 figures lock password, for owner use with the following requests: MLRecoverOwnerLockWithPassword, MLCreateNewUserWithName, MLChangeUserStatus, MLDeleteUserInLock, MLGetUsersFromLockWithPassword, MLUpdateSettingsForLockWithPassword ,MLChangeLockPassword
 * @param lockName - sets lock new name.
 * @param lockBrand - sets lock brand. to get lock brand (nullable), please see method MLGetBrandNameWithLockSN.
 * @param userName - owner user name.
 * @param userEmail - admin user email.
 * @param phoneNumber - owner user phone number (nullable).
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLSetOwnerLockWithPassword:(nonnull NSString *)password lockName:(nonnull NSString *)lockName lockBrand:(nullable NSString *)lockBrand userName:(nonnull NSString *)userName userEmail:(nonnull NSString *)userEmail userPhoneNumber:(nullable NSString *)phoneNumber andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;

/**
 * MLLock - locks current connected lock device.
 * @param callback - with NSError object and BOOL for success password
 */
-(void)MLLockWithCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLUnlock - unlocks current connected lock device.
 * @param callback - with NSError object and BOOL for success password
 */
-(void)MLUnlockWithCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLRecoverOwnerLockWithPassword - Recovers owner in current connected lock device. 
 * Please use this method in an event owner uses new/different iPhone. Please note, only one owner per lock, after owner recovers in new iPhone, owner capabilities in old iPhone device would be disabled.
 * @param Password - owner password.
 * @param callback - with NSError object and BOOL for success password
 */
- (void)MLRecoverOwnerLockWithPassword:(nonnull NSString *)Password withCallback:(void( ^  _Null_unspecified)(BOOL success,  NSError * _Nullable error))callback;


/**
 * MLCreateNewUserWithName - create new user for current connected lock device.
 * On request succeed, a new pending user (key) is created for the current connected lock. 
 * Then the lock is transmitting pending user (key) available, please see and MLListOfScanedLocks MLInitializedWithPendingKeyLocks for more information.
 * @param userName - new user name.
 * @param userRole - new user role (0-User, 1-Admin).
 * @param newUserPassword - pending user password, password is used in order to collect pending key from lock (using method: MLGetNewUserForLockWithPassword).
 * @param expirationTime - pending user expiration time in hours. After time passes, the pending key is revoked and can not be collected.
 * @param userPassword - current connected user to lock password.
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLCreateNewUserWithName:(nonnull NSString *)userName userRole:(NSInteger)userRole newUserPassword:(nonnull NSString *)newUserPassword expirationTime:(NSInteger)expirationTime userPassword:(nonnull NSString *)userPassword andCallback:(void(^ _Null_unspecified)(BOOL success,  NSError * _Nullable error))callback;

/**
 * MLGetNewUserForLockWithPassword - method is used in order to collect pending key from current connected lock device.
 * On request succeed, a pending user (key) is collected by user in iPhone, and user can perform MLLock/MLUnlock.
 * @param password - pending user password, same password (newUserPassword) in method: MLCreateNewUserWithName.
 * @param userName - new user name used to create pending user (key) in method: MLCreateNewUserWithName.
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLGetNewUserForLockWithPassword:(nonnull NSString *)password andUserName:(nonnull NSString *)userName andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;
    


/**
 * MLSetUserAdminWithPassword - method is used in order to set user as admin in current connected lock device.
 * Admin user permissions: MLLock, MLUnlock, MLCreateNewUserWithName, MLChangeUserStatus, MLDeleteUserInLock, MLGetLockStatusWithCallback.
 * Please note, method will succeed only if current user role (userRole) is admin (for more information please see: MLCreateNewUserWithName). 
 * @param newAdminPassword - new password, for admin user, that will be used in requests mentioned above.
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLSetUserAdminWithPassword:(nonnull NSString *)newAdminPassword withCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLDeleteUserInLock - method delete user (key) in current connected lock device.
 * Please note, only users with role admin/owner have permissions to perform MLDeleteUserInLock. Owner can delete admins/users, an admin can delete users.
 * @param name - user name to delete.
 * @param password - current connected user password.
 * @param callback - with NSError object and BOOL for success password.
 */

-(void)MLDeleteUserInLock:(nonnull NSString *)name withPassword:(nonnull NSString *)password andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;

/**
 * MLDeleteLock - method delete lock in app cache and storage memory.
 * Please note, this request does not involve lock request.
 * @param lock - representing specific lock to delete.
 * @param callback - with NSError object and BOOL for success password.
 * Use this method,
 */
-(void)MLDeleteLock:(nonnull LockModel *)lock andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;

/**
 * MLGetUsersFromLockWithPassword - method requests users (keys) list from current connected to lock (owner/admin have permission to get users from lock).
 * @param password - current connected to lock user password.
 * @param callback - with NSError object, BOOL for success password and keyList array with UserInLock objects. Please see UserInLock.h for more information.
 */
- (void)MLGetUsersFromLockWithPassword:(nonnull NSString *)password andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error, NSArray * _Nullable keyList))callback;


/**
 * MLChangeUserStatus - method enable/disable other user in current connected lock.
 * @param enable - BOOL value - for enable/disable user.
 * @param userName - user name to change status.
 * @param password - current connected to lock user password.
 * @param callback - with NSError object and BOOL for success password.
 */
-(void)MLChangeUserStatus:(BOOL)enable userName:(nonnull NSString *)userName withPassword:(nonnull NSString *)password andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLGetLockSerialNumberWithCallback - method gets current connected lock serial number. 
 * Please note, method callback can return with success, although lockSN is empty. It depends if lock firmware supports this request.
 * @param callback - with NSError object, BOOL for success password and lockSN (NSString * _Nullable).
 */
- (void)MLGetLockSerialNumberWithCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error,  NSString * _Nullable lockSN))callback;


/**
 * MLGetLockStatusWithCallback - method requests status from current connected lock.
 * Please note, only users with role admin/owner have permissions to perform MLGetLockStatusWithCallback.
 * @param callback - with NSError object, BOOL for success password and lockDevice (LockDevice *) representing the current connected lock.
 * On request succeed, get result using MLLockDevice public methods. For    0more information please see MLLockDevice.h.
 */
- (void)MLGetLockStatusWithCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error, LockDevice * _Nullable lockDevice))callback;


/**
 * MLUpdateSettingsForLockWithPassword - method update current connected lock settings - autoLock and mute. 
 * Please note, only users with role admin have permissions to perform MLUpdateSettingsForLockWithPassword.
 * @param lockPassword - current connected to lock owner password.
 * @param autoLock - BOOL value - for enable/disable autoLock.
 * @param mute - BOOL value - for enable/disable mute.
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLUpdateSettingsForLockWithPassword:(nonnull NSString *)lockPassword enableAutoLock:(BOOL)autoLock muteLock:(BOOL)mute withCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLUpdateSettingsForLockWithPassword - method changes current connected lock password.
 * Please note, only users with role admin have permissions to perform MLUpdateSettingsForLockWithPassword.
 * @param newPassword - new lock owner password.
 * @param currentPassword - current lock owner password.
 * @param callback - with NSError object and BOOL for success password.
 */
- (void)MLChangeLockPassword:(nonnull NSString *)newPassword currentPassword:(nonnull NSString *)currentPassword andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLGetBrandNameWithLockSN - server request that gets lock brand from lock serial number.
 * @param lockSN - lock serial number, please see: MLGetLockSerialNumberWithCallback for more information.
 * @param callback - with NSError object, BOOL for success password and brandName (NSString *).
 */
- (void)MLGetBrandNameWithLockSN:(NSString * _Nonnull)lockSN andCallback:(void(^ _Null_unspecified)(BOOL success, NSError * _Nullable error,  NSString * _Nullable  brandName))callback;


/**
 * MLRegisterLockOnlineWithCallback - server request to register current connected lock online.
 * @param callback - with NSError object, BOOL for success.
 */
- (void)MLRegisterLockOnlineWithCallback:(void (^ _Null_unspecified)(BOOL success, NSError * _Nullable error))callback;


/**
 * MLPasswordValidation - check password validation before set/edit password.
 * @param password - (NSString *).
 * @return - NSError, if NSError is null, MLPasswordValidation succeed.
 */
- (NSError * _Nullable )MLPasswordValidation:(nonnull NSString *)password;

/**
 * MLLockNameValidation - check Lock name validation before set lock name.
 * @param lockName - (NSString *).
 * @return - NSError, if NSError is null, MLPasswordValidation succeed.
 */
- (NSError * _Nullable )MLLockNameValidation:(nonnull NSString *)lockName;


/**
 * MLNameValidation - check user (key) name validation before set new user with name.
 * @param Name - (NSString *).
 * @return - NSError, if NSError is null, MLPasswordValidation succeed.
 */
- (NSError * _Nullable )MLNameValidation:(nonnull NSString *)Name;

@end
