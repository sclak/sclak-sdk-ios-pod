//
//  Feature.h
//  SclakApp
//
//  Created by albi on 12/02/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "FeaturePrivilege.h"

@protocol Feature
@end

@interface Features : ResponseObject

@property (nonatomic, strong)  NSArray <Optional, FeaturePrivilege> *privileges;

/**
 *  get feature privileges with group tag
 *
 *  @param groupTag the unique group tag
 *
 *  @return array list of privileges
 */
- (NSArray*) getPrivilegeForGroupTag:(NSString*)groupTag;

/**
 *  search first feature privilege for group tag that is not an included licence (purchased)
 *
 *  @param groupTag the unique group tag
 *
 *  @return YES if found, NO otherwise
 */
- (BOOL) hasPurchasedFeaturesForGroupTag:(NSString*)groupTag;

/**
 *  count feature privilege availabilities
 *
 *  @return -1 if feature privilege infinite found, a count otherwise
 */
- (NSNumber*) getAvailabilityForGroupTag:(NSString*)groupTag date:(NSDate*)currentDate;

/**
 * searches through every FeaturePrivilege, returns NO if at least one FeaturePrivilege is found
 * with expire time > currentDate
 * returns YES otherwise
 *
 *  @param currentDate NSDate representing 'now' to be compared with each feature privilege expire time
 *
 *  @return YES if all guest packs are expired, NO if at least one is non expired
 */
- (BOOL) guestPacksExpired:(NSDate*)currentDate;

/**
 *  returns the earlies expire time for found guest packs features
 *
 *  @return earliest expire time, or 0 if infinite guest pack found
 */
- (NSNumber*) latestGuestPacksExpireTime;

@end

@interface FeatureList : ResponseObject

@property (nonatomic, strong) Features <Optional> *list;

@end
