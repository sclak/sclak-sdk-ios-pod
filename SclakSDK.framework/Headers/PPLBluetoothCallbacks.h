//
//  PPLBluetoothCallbacks.h
//  SclakFramework
//
//  Created by Daniele Poggi on 26/01/2014.
//  Copyright © 2014 sclak. All rights reserved.
//

// CALLBACKS
typedef void(^BluetoothResponseCallback)(BOOL success, NSException *ex);
typedef void(^BluetoothResponseErrorCallback)(BOOL success, NSError *error);
typedef void(^BluetoothReadRSSICallback)(BOOL success, NSNumber *RSSI);
