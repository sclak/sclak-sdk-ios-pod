//
//  Period.h
//  SclakFacade
//
//  Created by Daniele Poggi on 16/05/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "TimeRange.h"

@protocol Period <NSObject>
@end

@interface Period : JSONModel

@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (nonatomic, strong) TimeRange *timeRange;

- (BOOL) hasSameTimeRange:(Period*)object;
- (BOOL) containsDay:(Day*)day;

- (NSString*)description;

- (Day*) beginDay;
- (TimeRange*) beginTimeRange;

- (Day*) endDay;
- (TimeRange*) endTimeRange;

- (TimeRange*) intermediateTimeRange;

@end