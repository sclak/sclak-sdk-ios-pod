//
//  Peripherals.h
//  Sclak2
//
//  Created by albi on 06/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Peripheral.h"

@interface Peripherals : ResponseObject

@property (nonatomic, strong) NSArray <Peripheral, Optional> *list;
@property (nonatomic, strong) NSNumber <Optional> *totalCount;

+ (BOOL) imAdminOrOwner:(NSArray*)peripherals;

+ (BOOL) atLeastOneGuestInPeripherals:(NSArray*)peripherals;

@end
