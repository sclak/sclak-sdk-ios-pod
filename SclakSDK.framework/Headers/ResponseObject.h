//
//  ResponseObject.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "JSONModel.h"

@interface ResponseObject : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *errorCode;
@property (nonatomic, strong) NSString <Optional> *errorMessage;

#pragma mark - Generic REST APIs

+ (void) getOne:(id)aid callback:(id)callback;
+ (void) getAllCallback:(id)callback;

+ (void) create:(id)model callback:(id)callback;
+ (void) update:(id)model callback:(id)callback;

+ (void) remove:(id)aid callback:(id)callback;

@end
