//
//  TDGenericFacade.h
//  SCLAK
//
//  Created by Daniele Poggi on 04/09/14.
//  Copyright (c) 2014 SCLAK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Keychain.h"

#define PREFS_AUTH_USERNAME                      @"PREFS_AUTH_USERNAME"
#define PREFS_AUTH_PHONE_NUMBER                  @"PREFS_AUTH_PHONE_NUMBER"
#define PREFS_AUTH_PASSWORD                      @"PREFS_AUTH_PASSWORD"

// DEFINE UTILS
#define PSTART                                  NSDate *startTime = [NSDate date]
#define PEND(...)                               NSLog(@"%@ - Time: %f", __VA_ARGS__, -[startTime timeIntervalSinceNow])
#define FACADE_EXCEPTION                        @"com.sclak.sclak.exception"

// KEYCHAIN
#define kKeychainUDIDKey                        @"com.sclak.keychain.udid"
#define KEYCHAIN_SERVICE_NAME                   @"AUTH_KEYCHAIN"

// APP GROUP
#define kAppGroupKey                            @"group.com.sclak.sclak"

@interface SCKGenericFacade : NSObject

#pragma mark Keychain

@property (nonatomic, readonly, strong) Keychain *keychain;

/**
 * the user password =_=
 */
@property (nonatomic, strong) NSString *password;

/**
 * the app group directory. if not provided, the facade will use application document directory as local storage
 */
@property (nonatomic, strong) NSURL *appGroupDirectory;
@property (nonatomic, assign) BOOL debug;

+ (instancetype) sharedFacade;

- (NSString*) documentDirectory;
- (NSURL*) applicationDocumentsDirectory;

#pragma mark - UUID

- (NSString*) getUUID;

#pragma mark - File Cache Management

- (id) parseFile:(NSString*)file;
- (void) writeString:(NSString*)stringModel toFile:(NSString*)file;
- (void) writeDictionary:(NSDictionary*)model toFile:(NSString*)file;
- (void) writeArray:(NSArray*)models toFile:(NSString*)file;
- (void) deleteFile:(NSString*)file;
- (void) deleteCacheFiles;

@end
