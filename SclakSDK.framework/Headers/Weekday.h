//
//  Weekday.h
//  SclakFacade
//
//  Created by Daniele Poggi on 21/03/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JSONModel.h"
#import "TimeRange.h"

@protocol Weekday
@end

@interface Weekday : JSONModel

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, strong) TimeRange *timeRange;

@property (nonatomic, strong) NSIndexPath <Ignore> *indexPath;

- (BOOL) hasSameTimeRange:(Weekday*)weekday;

@end