//
//  SclakSDK.h
//  SclakSDK
//
//  Created by Daniele Poggi on 24/01/2017.
//  Copyright © 2017 SCLAK s.r.l. All rights reserved.
//

#import "ResponseObject.h"
#import "User.h"
#import "Peripherals.h"
#import "PPLBluetoothCallbacks.h"

// API CALLBACKS
typedef void(^LoginCallback)(BOOL success, User *user, ResponseObject *failureObject);
typedef void(^PeripheralsCallback)(BOOL success, NSArray<Peripheral> *peripherals, ResponseObject *failureObject);

/**
 * SclakSDK main interface. It's a Singleton Class
 */
@interface SclakSDK : NSObject

/**
 * the user model, received by calling the Api loginWithUsername:password:callback
 */
@property (nonatomic, readonly, strong) User *user;

/**
 * contains the user locks, received by calling the Api getPeripheralsCallback:
 * each peripheral model contains the access privileges owned by the user
 * and the sent privileges that the user provided to others
 */
@property (nonatomic, readonly, strong) NSArray<Peripheral> *peripherals;

/*!
 @abstract
 retrieve the SclakSDK singleton shared instance
 @return the SclakSDK shared instance
 */
+ (instancetype) sharedInstance;

/*!
 @abstract
 Call this method from the [UIApplicationDelegate application:didFinishLaunchingWithOptions:]
 Initialize using project id, api token, secret.
 
 @param key The API key can be found in Project Settings under API Keys & Usage on Sclak Admin.
 @param secret The API secret can be found in Project Settings under API Keys & Usage on Sclak Admin.
 */
- (void) provideAPIKey:(NSString *)key APISecret:(NSString *)secret;

/*!
 @abstract
 log sclak user in with its username (email or phone number) and password
 if user is logged in, SDK statefully remembers the user and keep using its data
 
 @param username username (email or phone number)
 @param password password
 @param callback API callback, may return success or error with result
 */
- (void) loginWithUsername:(NSString *)username password:(NSString *)password callback:(LoginCallback)callback;

/*!
 @abstract
 retrieve logged user peripherals (keys)
 
 @param callback API callback, may return success with data or error with error_code and error_message (ResponseObject model)
        if the callback returns a success, the "id" result contains a "Peripherals" model
        if the callback returns a failure, the "id" result contains a "ResponseObject" model with error_code and error_message
 */
- (void) getPeripheralsCallback:(PeripheralsCallback)callback;

/*!
 @abstract
 check if an access can be done on peripheral at specified time
 
 @param peripheral one of the models received with getKeysCallback: API
 @param time specific opening time, can be in the past or future. pass "nil" if to use current timestamp
 @param callback API callback, may return success or error and response that specifies why the access cannot be done at specified time
 */
- (void) accessControlCheckWithKey:(Peripheral*)peripheral time:(NSDate*)time callback:(void (^)(BOOL result))callback;

/*!
 @abstract
 request an open command on peripheral
 before the request is sent, an access control check with current timestamp is performed. if the access control check fails,
 the open command is not issued.
 
 @param peripheral one of the models received with getKeysCallback: API
 @param callback API callback, may return success or error and response that specifies why the access cannot be done at specified time
 */
- (void) openSclakWithKey:(Peripheral*)peripheral callback:(BluetoothResponseErrorCallback)callback;

@end
