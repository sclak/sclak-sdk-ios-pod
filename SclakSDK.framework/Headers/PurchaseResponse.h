//
//  PurchaseResponse.h
//  SclakFacade
//
//  Created by Daniele Poggi on 25/01/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PurchaseResponse : ResponseObject

@property (nonatomic, strong) NSString *coupon_type;

@end