//
//  Peripheral.h
//  Sclak2
//
//  Created by albi on 06/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "PeripheralUser.h"
#import "Features.h"
#import "Firmware.h"
#import "SCKPeripheralType.h"
#import "PeripheralVersion.h"
#import "PeripheralSetting.h"
#import "PeripheralStatistics.h"
#import "AccessoryProtocol.h"
#import "PeripheralBaseModel.h"
#import "PeripheralProtocols.h"

#define edit_peripheral                             @"edit_peripheral"
#define add_peripheral                              @"add_peripheral"
#define get_peripheral                              @"get_peripheral"
#define use_peripheral                              @"use_peripheral"
#define create_user                                 @"create_user"
#define add_privilege                               @"add_privilege"
#define manage_groups                               @"manage_groups"
#define test_peripheral                             @"test_peripheral"
#define purchase_keys                               @"purchase_keys"
#define purchase_guest_packs                        @"purchase_guest_packs"
#define get_peripheral_usages                       @"get_peripheral_usages"
#define manage_pin_codes                            @"manage_pin_codes"
#define pair_peripheral                             @"pair_peripheral"
#define manage_peripherals                          @"manage_peripherals"
#define manage_users                                @"manage_users"
#define replace_peripheral                          @"replace_peripheral"
#define manage_firmwares                            @"manage_firmwares"
#define bulk_add_coupons                            @"bulk_add_coupons"
#define otau_peripheral                             @"otau_peripheral"

#define kGroupTagSuperAdmin                         @"super_admin"
#define kGroupTagInstaller                          @"installer"
#define kGroupTagAdmin                              @"admin"
#define kGroupTagOwner                              @"owner"
#define kGroupTagGuest                              @"guest"

// PERIPHERAL TYPES
#define PERIPHERAL_TYPE_SCLAK                                       @"sclak"
#define PERIPHERAL_TYPE_ULOCK                                       @"ulock"
#define PERIPHERAL_TYPE_PARKEY                                      @"parkey"
#define PERIPHERAL_TYPE_NEAR                                        @"near"
#define PERIPHERAL_TYPE_WITTKOPP                                    @"wittkopp"
#define PERIPHERAL_TYPE_SAFESCLAK_BS                                @"sclaklock_bs"
#define PERIPHERAL_TYPE_SCLAK_KEYBOARD                              @"keyb"
#define PERIPHERAL_TYPE_SCLAK_FOB                                   @"keyfob"
#define PERIPHERAL_TYPE_SCLAK_SAFE                                  @"sclaksafe"
#define PERIPHERAL_TYPE_SCLAK_LIGHT_DIMMER                          @"lightdimm"
#define PERIPHERAL_TYPE_ZTF                                         @"ZTF"
#define PERIPHERAL_TYPE_SAFESCLAK_MS                                @"sclaklock_ms"
#define PERIPHERAL_TYPE_CEMO                                        @"cemo"
#define PERIPHERAL_TYPE_SCLAK_BATTERY                               @"sclakb"
#define PERIPHERAL_TYPE_ODL                                         @"odl"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_LEGACY                         @"handle"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_TAG                            @"h_tag"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_KEYBOARD                       @"h_keyb"
#define PERIPHERAL_TYPE_SCLAK_TAG                                   @"s_tag"
#define PERIPHERAL_TYPE_SCLAK_MIFARE                                @"mifare"
#define PERIPHERAL_TYPE_ENTR                                        @"entr"

// THIRD-PARTY CUSTOMIZATION
#define SCLAK_PERIPHERAL_VERSION_CODE_DIERRE                        @"dierre"
#define SCLAK_PERIPHERAL_VERSION_CODE_VALNES                        @"valnes"
#define SCLAK_PERIPHERAL_VERSION_CODE_CEMO                          @"cemo"
#define SCLAK_PERIPHERAL_VERSION_CODE_GARDESA_PORTE                 @"gardesa_porte"
#define SCLAK_PERIPHERAL_VERSION_CODE_AGB                           @"agb"
#define SCLAK_PERIPHERAL_VERSION_CODE_KAITHRON                      @"kaithron_air"

#define violetTag                                   @"violet"
#define lightVioletTag                              @"lightViolet"
#define oliveTag                                    @"olive"
#define lightGreenTag                               @"lightGreen"
#define blueTag                                     @"blue"
#define turquoiseTag                                @"turquoise"

@interface Peripheral : PeripheralBaseModel

@property (nonatomic, strong) NSString<Optional> *btcode;
@property (nonatomic, strong) NSString<Optional> *lot;
@property (nonatomic, strong) NSString<Optional> *secretCode;

@property (nonatomic, strong) SCKPeripheralType<Optional> *peripheralType;
@property (nonatomic, strong) NSNumber<Optional> *peripheralTypeId;
@property (nonatomic, strong) NSString<Optional> *peripheralTypeCode;

@property (nonatomic, strong) PeripheralVersion<Optional> *peripheralVersion;

@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *address;
@property (nonatomic, strong) NSNumber<Optional> *enabled;
@property (nonatomic, strong) NSString<Optional> *desc;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *activationTime;

// PERIPHERAL GROUPS
@property (nonatomic, strong) NSArray<PeripheralGroup, Optional> *peripheralGroups;

// INSTALL
@property (nonatomic, strong) NSNumber<Optional> *longitude;
@property (nonatomic, strong) NSNumber<Optional> *latitude;

// AUTO-OPEN AND KNOCK KNOCK
@property (nonatomic, strong) NSNumber<Optional> *autoOpen;
@property (nonatomic, strong) NSNumber<Optional> *autoOpenProximity;
@property (nonatomic, strong) NSNumber<Optional> *knockKnock;
@property (nonatomic, strong) NSNumber<Optional> *autoOpenFired;
@property (nonatomic, strong) NSDate<Optional> *autoOpenTime;
@property (nonatomic, strong) NSNumber<Optional> *knockKnockFired;
@property (nonatomic, strong) NSDate<Optional> *knockKnockTime;

// FIRMWARE
@property (nonatomic, strong) NSString<Optional> *peripheralFirmwareVersion; // used for PUT
@property (nonatomic, strong) Firmware<Optional> *peripheralFirmware;

// FEATURES
@property (nonatomic, strong) Features <Optional> *features;
@property (nonatomic, strong) NSString<Optional> *timeZone;

// SETTINGS
@property (nonatomic, strong) NSMutableArray<PeripheralSetting, Optional> *peripheralSettings;

// ACCESSORIES
@property (nonatomic, strong) NSNumber<Optional> *pairedPeripheralsCount;
@property (nonatomic, strong) NSArray<Accessory, Optional> *accessories;

// PIN CODES
@property (nonatomic, strong) NSNumber<Optional> *pinSyncNeeded;

// STATISTICS
@property (nonatomic, strong) PeripheralStatistics<Optional> *peripheralStatistics;

// peripheral "check_only" flag, used in Peripheral PUT
@property (nonatomic, strong) NSNumber<Optional> *checkOnly;

// USE ONLY BY APP
@property (nonatomic, strong) NSNumber<Ignore> *changed;
@property (nonatomic, strong) NSNumber<Ignore> *isDiscovered;
@property (nonatomic, strong) NSDate<Optional> *manualOpenTime;

// COLORS
@property (nonatomic, strong) NSString<Optional> *colorTag;

// MODIFIED DICTIONARY
@property (nonatomic, strong) NSDictionary <Ignore> *modifiedFieldsDictionary;

- (NSString*) presentationName;

/**
 * this method removes from sentPrivileges my invitation
 * and put it in mySentPrivilege proprerty
 */
- (void) patchPrivilegesWithUserId:(NSNumber*)userId;

- (BOOL) added;
- (BOOL) used;
- (BOOL) active;
- (BOOL) tested;

- (NSUInteger) countPrivilegesForGroupTag:(NSString*)groupTag;
- (NSUInteger) countPrivilegesForGroupTag:(NSString*)groupTag privileges:(NSArray*)privileges;

- (UIColor*) getColorForTag:(NSString*)colorTag;

#pragma mark - Getter for peripheral + group sent privileges

/**
 * return an array containing all sent privileges for this specific peripheral
 * and all sent privileges for all the peripheral groups that contain this peripheral
 */
- (NSArray*) allSentPrivileges;

#pragma mark - Getters for specific peripheral usages

- (BOOL) isPinPukSupported;
- (BOOL) isLatching;
- (BOOL) isSlowBluetoothAnnounce;
- (BOOL) doesSupportPeripheralModes;
- (BOOL) hasPairedKeyboard;

#pragma mark - Peripheral Settings

- (NSNumber*) buzzer;
- (void) setBuzzer:(NSNumber<Ignore>*)value;

- (NSNumber*) led;
- (void) setLed:(NSNumber<Ignore>*)value;

- (NSNumber*) autocloseTime;
- (void) setAutocloseTime:(NSNumber<Ignore>*)value;

- (NSString*) peripheralMode;
- (void) setPeripheralMode:(NSString<Ignore>*)value;

- (BOOL) ownerPostPrivilegeEnabled;
- (void) setOwnerPostPrivilegeEnabled:(BOOL)value;

- (BOOL) ownerPostPrivilegePush;
- (void) setOwnerPostPrivilegePush:(BOOL)value;

- (BOOL) adminManageOwnerPrivilegesEnabled;
- (void) setAdminManageOwnerPrivilegesEnabled:(BOOL)value;

- (BOOL) supportOwnerPostPrivilege;
- (BOOL) supportAutoOpen;
- (BOOL) supportTocToc;

#pragma mark - Specific Handle Settings

- (NSNumber*) buzzOnHandleEnabled;
- (void) setBuzzOnHandleEnabled:(NSNumber<Ignore>*)value;

- (NSNumber*) buzzOnHandleDisabled;
- (void) setBuzzOnHandleDisabled:(NSNumber<Ignore>*)value;

@end
