//
//  Purchases.h
//  SclakApp
//
//  Created by albi on 30/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Purchase.h"

@interface Purchases : ResponseObject

@property (nonatomic, strong) NSArray <Purchase> *list;

@end
