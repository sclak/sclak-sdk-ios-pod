//
//  SCKFacade.h
//  Sclak2
//
//  Created by albi on 04/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "SCKNetworkFacade.h"
#import "AFDownloadRequestOperation.h"

// Callbacks
#import "SCKFacadeCallbacks.h"

// Models
#import "Common.h"
#import "Device.h"
#import "ResponseObject.h"
#import "AuthToken.h"
#import "Peripheral.h"
#import "Peripherals.h"
#import "PeripheralGroup.h"
#import "User.h"
#import "Coupon.h"
#import "Group.h"
#import "Privilege.h"
#import "PurchaseReceipt.h"
#import "Purchases.h"
#import "GetPeripheralsOptions.h"
#import "PinCode.h"
#import "PukCode.h"
#import "Usage.h"
#import "PrivilegeEmailTemplate.h"
#import "PeripheralActivation.h"
#import "PrivilegeActivation.h"
#import "TinyUrl.h"
#import "PrivilegeResetted.h"
#import "SclakTime.h"
#import "PeripheralSecret.h"
#import "Calendar.h"
#import "PostPurchaseCoupon.h"
#import "Accessory.h"
#import "Installation.h"
#import "InstallationPost.h"
#import "LogUsage.h"
#import "PurchaseResponse.h"
#import "Project.h"

#import "PinManager.h"

#define F                                       [SCKFacade sharedFacade]

#define authTokensUrl                           @"/auth_tokens"
#define groupsUrl                               @"/groups"
#define updateUserUrl                           @"/users"
#define userEmailsUrl                           @"/users_emails"
#define activateUserEmailUrl                    @"/users_emails/activate"
#define sendActivationCodeUrl                   @"/users_emails/send_activation_code"
#define generateResetPasswordUrl                @"/users/generate_reset_password_code"
#define changePasswordUrl                       @"/users/change_password"
#define resetPasswordUrl                        @"/users/reset_password"
#define check_operative_code_post               @"/users/check_operative_code_sha"
#define activatePeripheralUrl                   @"/peripherals/%@/activate"
#define peripheralsUrl                          @"/peripherals"
#define peripheralUrl                           @"/peripherals/%@"
#define peripheralSecretCodeUrl                 @"/peripherals/%@/get_secret_code"
#define peripheralAvailableBtcodeUrl            @"/peripherals/available_btcode"
#define peripheralSetSecretCodeUrl              @"/peripherals/%@/set_secret_code"
#define peripheralGroupsUrl                     @"/peripherals_groups"
#define privilegesIdUrl                         @"/privileges/%@"
#define privilegesUrl                           @"/privileges"
#define privilegeUsersUrl                       @"/privileges/sent"
#define couponUrl                               @"/coupons"
#define activatePrivilegeUrl                    @"/privileges/%@/activate"
#define devicesUrl                              @"/devices"
#define privilegeConfirm                        @"/privileges/%@/confirm"
#define privilegeDeny                           @"/privileges/%@/deny"
#define privilegeSetInviteExpireTime            @"/privileges/%@/set_invite_expire_time"
#define requireConfirm                          @"/privileges/%@/require_confirm"
#define purchasesUrl                            @"/purchases"
#define privilegesDisable                       @"/privileges/%@/disable"
#define peripheralsLog                          @"/peripherals/%@/log_usage"
#define peripheralsUsagesLog                    @"/peripherals/log_usages"
#define peripheralsUsages                       @"/peripherals/%@/usages"
#define firmwaresUrl                            @"/firmwares"
#define latestfirmwareUrl                       @"/firmwares/last_version_for_peripheral_type/"

// pin codes
#define peripheralPinCodes                      @"/peripherals/%@/pin_codes"
#define pinCodesUrl                             @"/pin_codes"
#define pinCodeUrl                              @"/pin_codes/%@"
#define resetPinCodesUrl                        @"/pin_codes/%@/reset"
#define pinCodeBulkUrl                          @"/pin_codes/bulk"
#define peripheralPinCodeAvailable              @"/peripherals/%@/available_pin_code"

// pair unpair
#define pairPeripheral                          @"/peripherals/%@/pair/%@"
#define unpairPeripheral                        @"/peripherals/%@/unpair"

// template
#define privilegesEmailTemplate                 @"/privileges/email_template"

// tiny url
//#define tinyUrl                                 @"/urls/tiny_activation_code"
#define tinyUrlService                          @"https://tinyurl.com/api-create.php?url=%@"

// user pin codes
#define userPinCodesIdUrl                       @"/user_pin_codes/%@"
#define userPinCodesUrl                         @"/user_pin_codes"

#define requestResetOperativeCodeUrl            @"/users/request_reset_operative_code"
#define resetOperativeCodeUrl                   @"/users/reset_operative_code"

#define privilegesActivateResetUrl              @"/privileges/%@/activate_reset"

#define timeUrl                                 @"/time"

// ERRORS
#define ERROR_INVALID_TOKEN                     2
#define ERROR_DEVICE_NOT_FOUND                  30
#define ERROR_ATTEMPTS_USED                     32
#define ERROR_RESET_PASSWORD_ATTEMPTS_USED      44
#define ERROR_DEVICE_DISABLED                   45
#define ERROR_USER_EMAIL_NOT_ACTIVE             47
#define ERROR_PIN_CODES_NOT_AVAILABLE           71
#define ERROR_PUK_CODE_EXIST                    75
#define ERROR_PIN_CODE_EXIST                    67

// USER DEFAULTS
#define PREFS_AUTH_USERID                        @"PREFS_AUTH_USERID"
#define PREFS_PUSH_TOKEN                         @"PREFS_PUSH_TOKEN"
#define kStandardUserDefaultsAlreadyLoggedInOnce @"kStandardUserDefaultsAlreadyLoggedInOnce"
#define kSettingsDictKey                         @"settingsDictKey"
#define kPasscode                                @"passcode"
#define kLockList                                @"lockList"
#define kTouchID                                 @"touchID"
#define kShowHints                               @"showHints"
#define kShowLowBattery                          @"kShowLowBattery"
#define kHideDisabledSclak                       @"kHideDisabledSclak"
#define kActivationCode                          @"activationCode"
#define kPrivilegeId                             @"privilegeId"
#define kSettingsHideSclakAddresses              @"kSettingsHideSclakAddresses"

// NOTIFICATIONS
#define kFacadeDidLoginNotification             @"kFacadeDidLoginNotification"
#define kFacadeDidLogoutNotification            @"kFacadeDidLogoutNotification"
#define kFacadePeripheralsUpdatedNotification   @"kFacadePeripheralsUpdatedNotification"
#define kFacadeCleanNotification                @"kFacadeCleanNotification"

typedef NS_ENUM(NSUInteger, TinyUrlType) {
    TinyUrlTypeActivationCode,
    TinyUrlTypeResetKey,
};

@interface SCKFacade : SCKNetworkFacade

#pragma mark - Facade

+ (SCKFacade*) sharedFacade;

#define VERIFICATION_CODE_FIELD_LENGTH 1
#define VERIFICATION_CODE_NUMBER_FIELD 4
#define VERIFICATION_CODE_LENGTH VERIFICATION_CODE_FIELD_LENGTH * VERIFICATION_CODE_NUMBER_FIELD

// CACHES
@property (nonatomic, strong) NSArray *users;
@property (nonatomic, strong) NSArray *userDevicesCache;
    
@property (nonatomic, strong) NSMutableArray *peripherals;
@property (nonatomic, strong) NSMutableDictionary *peripheralsCache;
    
@property (nonatomic, strong) NSMutableArray *peripheralGroups;
@property (nonatomic, strong) NSMutableDictionary *peripheralGroupsCache;
    
@property (nonatomic, strong) NSDictionary *settings;
@property (nonatomic, strong) NSDictionary *iapProducts;
@property (nonatomic, strong) Firmware *latestFirmware;

@property (nonatomic, strong) Project *project;

// INSTALLER
@property (nonatomic, strong) NSMutableArray *installerEnabledIndexes;

// LOGIN ERROR CALLBACK
@property (nonatomic, strong) LoginErrorCallback loginErrorCallback;

#pragma mark - Caches APIs

/**
 *  deserialize User, Profile, Peripherals, User Devices, Firmware caches
 */
- (void) deserializeCaches;

#pragma mark User APIs

- (void) getUserProfileCallback:(ResourceCallback)callback;
- (void) updateUserProfile:(User*)user callback:(ResourceCallback)callback;
- (void) addUserEmail:(NSString*)email callback:(ResourceCallback)callback;
- (void) deleteUserEmail:(NSString*)email callback:(ResourceCallback)callback;
- (void) activateUser:(NSString*)email activationCode:(NSString*)code callback:(ResourceCallback)callback;
- (void) sendActivationCodeForEmail:(Email*)email callback:(ResourceCallback)callback;
- (void) requestInviteExpireTimeForPrivilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) logoutCallback:(ResourceCallback)callback;

#pragma mark Peripheral APIs

- (void) getPeripheralsCallback:(ResourceCallback)callback;
- (void) getPeripheralsWithEditTime:(NSNumber*)editTime callback:(ResourceCallback)callback;
- (void) getPeripheralsWithOptions:(GetPeripheralsOptions*)options callback:(ResourceCallback)callback;

- (Peripheral*) getPeripheralWithBtcode:(NSString*)btcode;
- (void) getPeripheralWithBtcode:(NSString*)btcode enableCache:(BOOL)enableCache callback:(ResourceCallback)callback;
- (void) getPeripheralWithBtcode:(NSString*)btcode andConfirmCode:(NSString*)code enableCache:(BOOL)enableCache callback:(ResourceCallback)callback;
- (void) getSecretForPeripheral:(Peripheral*)peripheral sequence:(NSNumber*)sequence callback:(ResourceCallback)callback;
    
/**
 * activate a peripheral with a code
 *
 * @param btcode - the peripheral btcode
 * @param code - the coupon code
 * @return model of kind Activation if success, ResponseObject otherwise
 */
- (void) activatePeripheralWithBtcode:(NSString*)btcode andActivationCode:(NSString*)code callback:(ResourceCallback)callback;
- (void) updatePeripheral:(Peripheral*)peripheral callback:(ResourceCallback)callback;
- (void) updatePeripheral:(Peripheral*)peripheral writeCache:(BOOL)writeCache callback:(ResourceCallback)callback;

- (Peripheral*) getPeripheralWithPrivilegeId:(NSNumber*)privilegeId;
- (Privilege*) getPrivilegeWithId:(NSNumber*)privilegeId forPeripheral:(Peripheral*)peripheral;

/**
 * btcode: la periferica da mettere in pair con lo sclak (e.g. tastiera)
 * destinationBtcode: il btcode dello sclak
 */
- (void) pairPeripheralBtcode:(NSString*)btcode withDestinationBtcode:(NSString*)destinationBtcode callback:(ResourceCallback)callback;
- (void) unpairPeripheralBtcode:(NSString*)btcode callback:(ResourceCallback)callback;
- (void) getAvailablePinCodeForBtCode:(NSString*)btcode callback:(ResourceCallback)callback;

/**
 * updates the peripheral cache by adding or updating peripheral model
 */
- (BOOL) updatePeripheralsCache:(Peripheral*)peripheral;

/**
 * removes from the cache all the peripherals that are in current cache but NOT in *all* array
 * if *all* array is null nothing is removed.
 */
- (void) updatePeripheralsCacheRemoveDeleted:(NSArray*)allPeripherals;

#pragma mark Peripheral Group APIs
    
- (PeripheralGroup*) getPeripheralGroupWithId:(NSNumber*)peripheralGroupId;
- (PeripheralGroup*) getPeripheralGroupWithTag:(NSString*)tag;
- (void) updatePeripheralGroupCache:(PeripheralGroup*)peripheralGroup;
- (void) removePeripheralGroupFromCache:(PeripheralGroup*)peripheralGroup;
    
#pragma mark Operative code APIs

- (void) checkOperativeCode:(NSString*)cypheredOperativeCode callback:(ResourceCallback)callback;
- (void) postRequestResetOperativeCodeWithHash:(NSString*)opcodeHash email:(NSString*)email disableEmail:(BOOL)disableEmail disablePush:(BOOL)disablePush callback:(ResourceCallback)callback;
- (void) postResetOperativeCodeWithHash:(NSString*)opcodeHash resetCode:(NSString*)resetCode disableMassEmail:(BOOL)disableMassEmail disableMassPush:(BOOL)disableMassPush callback:(ResourceCallback)callback;

#pragma mark Coupons APIs

/**
 * get coupon model with code
 *
 * @param coupon - the coupon code
 * @return model of kind Coupon if success, ResponseObject otherwise
 */
- (void) getCoupon:(NSString*)coupon callback:(ResourceCallback)callback;
- (void) createCoupon:(NSString*)couponType callback:(ResourceCallback)callback;

#pragma mark - Privileges APIs

- (void) getPrivilegeWithId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) activatePrivilege:(NSNumber*)privilegeId code:(NSString*)code callback:(ResourceCallback)callback;
- (void) activatePrivilege:(NSNumber*)privilegeId code:(NSString*)code activateEmail:(BOOL)activateEmail callback:(ResourceCallback)callback;
- (void) privilegeREST:(Privilege*)privilege action:(ActionREST)action callback:(ResourceCallback)callback;
- (void) disablePrivilege:(Privilege*)privilege callback:(ResourceCallback)callback;
- (void) putActivateReserForPrivilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;

#pragma mark - Group APIs

- (void) manageGroupREST:(Group*)group action:(ActionREST)action callback:(ResourceCallback)callback;

#pragma mark - Devices APIs

- (void) getDevicesCallback:(ResourceCallback)callback;
- (void) putDeviceWithPushToken:(NSString*)pushToken enabled:(NSNumber*)enable callback:(ResourceCallback)callback;
- (void) putDeviceWithPushToken:(NSString*)pushToken udid:(NSString*)udid enabled:(NSNumber*)enabled callback:(ResourceCallback)callback;

#pragma mark - Entryphone

- (void) requireConfirmForPrivilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) actionForPrivilege:(PrivilegeAction)action forPrivilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;

#pragma mark - Purchase

- (void) addPurchase:(PurchaseReceipt*)purchase callback:(ResourceCallback)callback;
- (void) postPurchaseCoupon:(PostPurchaseCoupon*)model callback:(ResourceCallback)callback;
- (void) getPurchasesCallback:(ResourceCallback)callback;

#pragma mark - Firmware

- (void) getFirmwaresCallback:(ResourceCallback)callback;
- (void) getFirmwaresWithCode:(NSString*)code callback:(ResourceCallback)callback;
- (void) downloadFirmware:(Firmware*)model progress:(void (^)(AFDownloadRequestOperation *operation, NSInteger bytesRead, long long totalBytesRead, long long totalBytesExpected, long long totalBytesReadForFile, long long totalBytesExpectedToReadForFile))block callback:(ResourceCallback)callback;
- (void) getLatestFirmware:(NSString*)peripheralTypeCode callback:(ResourceCallback)callback;

#pragma mark - Collaudo

- (void) getAvailableBtcodeWithFirmwareType:(NSNumber*)firmwareType callback:(ResourceCallback)callback;
- (void) remoteInitSkWithBtode:(NSString*)btcode sequence:(NSNumber*)sequence romId:(NSString*)romId initialCode:(NSString*)initialCode callback:(ResourceCallback)callback;

#pragma mark - Pin Code

- (void) getPinCodesForBtCode:(NSString*)btcode callback:(ResourceCallback)callback;
- (void) getPinCodeWithId:(NSNumber*)pinCodeId callback:(ResourceCallback)callback;
- (void) putPinCode:(PinCode*)pinCode callback:(ResourceCallback)callback;
- (void) postPinCode:(PinCode*)pinCode callback:(ResourceCallback)callback;
- (void) postPinCodes:(NSArray*)codes forPeripheralId:(NSNumber*)peripheralId flags:(NSString*)flags command:(NSString*)command callback:(ResourceCallback)callback;
- (void) deletePinCode:(NSNumber*)pinCodeId callback:(ResourceCallback)callback;
- (void) resetPinCodes:(NSString*)btcode callback:(ResourceCallback)callback;

#pragma mark - User Pin Codes

- (void) getUserPinCodeForId:(NSNumber*)userPinCodeId callback:(ResourceCallback)callback;
- (void) getUserPinCodesForPrivilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) postUserPinCode:(NSString*)userPinCode  privilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) postUserPinCodes:(NSArray*)userPinCodes privilegeId:(NSNumber*)privilegeId callback:(ResourceCallback)callback;
- (void) putUserPinCode:(PinCode*)userPinCode callback:(ResourceCallback)callback;
- (void) deleteUserPinCode:(NSString*)cipheredUserPin callback:(ResourceCallback)callback;

#pragma mark - Email Template

- (void) getPrivilegeTemplateEmailWithCallback:(ResourceCallback)callback;

#pragma mark - Tiny Url

- (void) tinyUrlForCode:(NSString*)code clearPuk:(NSString*)puk clearPin:(NSString*)pin type:(TinyUrlType)type webUrlNeeded:(BOOL)webUrlNeeded webUrlTiny:(BOOL)webUrlTiny callback:(ResourceCallback)callback;
- (void) tinyUrlForCode:(NSString*)code clearPuks:(NSDictionary*)puks pinGroup:(NSDictionary*)pinGroup type:(TinyUrlType)type webUrlNeeded:(BOOL)webUrlNeeded webUrlTiny:(BOOL)webUrlTiny callback:(ResourceCallback)callback;

#pragma mark - Time

- (void) getTimeCallback:(ResourceCallback)callback;

#pragma mark - Error Management

- (void) manageError:(ResponseObject*)responseObject callback:(ResourceCallback)callback;

@end
