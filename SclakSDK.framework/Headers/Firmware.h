//
//  Firmware.h
//  ULockUnit
//
//  Created by Daniele Poggi on 27/01/15.
//  Copyright (c) 2015 SCLAK. All rights reserved.
//

#import "ResponseObject.h"

typedef NS_ENUM(NSUInteger, FirmwarePeripheralType) {
    FirmwarePeripheralTypeUnknown,
    FirmwarePeripheralTypeUlock,
    FirmwarePeripheralTypeSclak,
    FirmwarePeripheralTypeParkey,
    FirmwarePeripheralTypeHartmann
};

@protocol Firmware

@end

@interface Firmwares : ResponseObject

@property (nonatomic, strong) NSMutableArray<Firmware> *list;

@end

@interface Firmware : ResponseObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSNumber <Optional> *supportPin;
@property (nonatomic, strong) NSNumber <Optional> *supportUpgrade;
@property (nonatomic, strong) NSString <Optional> *lastUpgradeVersion;
@property (nonatomic, strong) NSString<Optional> *checksum;
@property (nonatomic, strong) NSString<Optional> *notes;
@property (nonatomic, strong) NSNumber <Optional> *insertTime;
@property (nonatomic, strong) NSNumber <Optional> *editTime;
@property (nonatomic, strong) NSString *peripheralTypeCode;

- (FirmwarePeripheralType) firmwareType;
- (NSString*) filename;
- (BOOL) updateRequiredForPeripheralVersion:(NSString*)peripheralVersion;

@end
