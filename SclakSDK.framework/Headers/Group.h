//
//  Group.h
//  SclakApp
//
//  Created by Daniele Poggi on 20/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "JSONModel.h"

@protocol Group
@end

@interface Group : JSONModel

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *groupTag;
@property (nonatomic, strong) NSDate *insertTime;

@end

@interface Groups : JSONModel

@property (nonatomic, strong) NSArray<Group> *list;

@end
