//
//  InstallationPost.h
//  SclakFacade
//
//  Created by Daniele Poggi on 20/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface InstallationPost : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *installerId;
@property (nonatomic, strong) NSString *installerEmail;

@property (nonatomic, strong) NSNumber<Optional> *adminId;
@property (nonatomic, strong) NSString *adminEmail;

@property (nonatomic, strong) NSMutableArray *btcodes;

@end