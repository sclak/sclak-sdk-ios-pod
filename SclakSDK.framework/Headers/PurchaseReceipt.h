//
//  Purchase.h
//  SclakApp
//
//  Created by albi on 27/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PurchaseReceipt : ResponseObject

@property (nonatomic, strong) NSString *productIdentifier;
@property (nonatomic, strong) NSString *receipt;
@property (nonatomic, strong) NSString *peripheralTypeCode;
@property (nonatomic, strong) NSNumber <Optional> *peripheralId;

@end
