//
//  Keychain.h
//  SclakApp
//
//  Created by albi on 07/02/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Common.h"

@interface Keychain : NSObject {
    NSString * service;
    NSString * group;
}

-(id)initWithService:(NSString *)aService withGroup:(NSString*)aGroup;

-(BOOL)insert:(NSString *)key withData:(NSData *)data;
-(BOOL)update:(NSString*)key withData:(NSData*)data;
-(BOOL)upsert:(NSString *)key withData:(NSData *)data;
-(BOOL)remove:(NSString*)key;
-(NSData*)find:(NSString*)key;

@end
