//
//  PeripheralBaseModel.h
//  SclakFacade
//
//  Created by Daniele Poggi on 01/08/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Privilege.h"

@interface PeripheralBaseModel : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSArray<Optional> *groupTags;
@property (nonatomic, strong) NSMutableArray<Privilege, Optional> *sentPrivileges;
@property (nonatomic, strong) NSArray <Privilege, Optional> *privileges;

- (BOOL) isPrivilegeGuest;
- (BOOL) isPrivilegeOwner;
- (BOOL) isPrivilegeAdmin;
- (BOOL) isPrivilegeInstaller;
- (BOOL) isPrivilegeSuperAdmin;

- (BOOL) isPrivilegesPendingReset;

- (BOOL) isOnlyInstaller;
- (BOOL) isOnlyGuest;

- (BOOL) hasAdmin;
- (BOOL) allPrivilegesDeleted;

- (Privilege*) getPrivilegeForId:(NSNumber*)privilegeId;
- (Privilege*) getSentPrivilegeForId:(NSNumber*)privilegeId;
- (Privilege*) getPrivilegeForGroupTag:(NSString*)groupTag;

- (BOOL) can:(NSString*)action withData:(Constraint*)data;
- (BOOL) can:(NSString*)action withData:(Constraint*)data privilege:(Privilege**)privilege;

@end
