//
//  User.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Email.h"
#import "SCKFacadeCallbacks.h"

@interface User : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSString <Optional> *surname;
@property (nonatomic, strong) NSString <Optional> *phoneNumber;
@property (nonatomic, strong) NSArray <Email, Optional> *emails;
@property (nonatomic, strong) NSString <Optional> *primaryEmail;
@property (nonatomic, strong) NSString <Optional> *password;
@property (nonatomic, strong) NSNumber <Optional> *otauBetaEnabled;
@property (nonatomic, strong) NSNumber <Optional> *opcodeSetted;
@property (nonatomic, strong) NSString <Optional> *opcodeHash;
@property (nonatomic, strong) NSNumber <Optional> *ghost;

- (NSString*) getName;
- (NSString*) getSurname;
- (NSString*) fullName;

- (NSString*) primaryEmail;

- (BOOL) emailVerified;

#pragma mark - APIs

+ (void) changePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword callback:(ResourceCallback)callback;
+ (void) generateResetPasswordCode:(Email*)email callback:(ResourceCallback)callback;
+ (void) resetPasswordForEmail:(Email*)email code:(NSString*)code password:(NSString*)password callback:(ResourceCallback)callback;

@end
