//
//  PeripheralSecret.h
//  SclakFacade
//
//  Created by albi on 12/11/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PeripheralSecret : ResponseObject

@property (nonatomic, strong) NSString *secretCode;

@end
