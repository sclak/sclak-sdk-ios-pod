//
//  Usage.h
//  SclakApp
//
//  Created by albi on 10/03/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"
#import "Device.h"

@protocol Usage
@end

@interface Usage : JSONModel

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSNumber<Optional> *anonymous;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;

@end

@interface Usages : ResponseObject

@property (nonatomic, strong) NSArray <Optional, Usage> *list;
@property (nonatomic, strong) NSNumber <Optional> *nextPage;

@end