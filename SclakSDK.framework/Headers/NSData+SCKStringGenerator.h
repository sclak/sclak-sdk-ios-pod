//
//  NSData+PPLStringGenerator.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 10/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (SCKStringGenerator)

- (NSString*) hexDataToString;
- (NSString*) MD5String;

@end
