//
//  TDNetworkFacade.h
//  SCLAK
//
//  Created by Daniele Poggi on 04/09/14.
//  Copyright (c) 2014 SCLAK. All rights reserved.
//

#import "SCKGenericFacade.h"
#import "SCKFacadeCallbacks.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFNetworkReachabilityManager.h"
#import "User.h"
#import "AuthToken.h"

// NETWORK
#define DEFAULT_DEBUG_POST_REQUESTS             YES
#define DEFAULT_DEBUG_POST_RESPONSES            YES
#define ERROR_DOMAIN                            @"com.sclak.error"

#define API_DOMAIN                              @"api.sclak.com"
#define API_DOMAIN_REACHABLE                    @"api.sclak.com"
#define API_DEVELOPMENT                         @"https://api-test.sclak.com"
#define API_INTEGRATION                         @"https://api-intgr.sclak.com"
#define API_PRODUCTION                          @"https://api.sclak.com"

#if defined USE_DEVELOPMENT
    #define DEFAULT_API_URL                         API_DEVELOPMENT
#elif defined USE_INTEGRATION
    #define DEFAULT_API_URL                         API_INTEGRATION
#else
    #define DEFAULT_API_URL                         API_PRODUCTION
#endif

#define kAuthToken                                  @"kAuthToken"
#define kAuthTokenExpiryDate                        @"kAuthTokenExpiryDate"

#define IS_LOGGED                                   @"IS_LOGGED"

@interface SCKNetworkFacade : SCKGenericFacade

@property (nonatomic, assign) ApiConfig apiConfig;

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;
@property (strong, nonatomic) AFHTTPRequestOperationManager *stringManager;
@property (nonatomic, strong) AFNetworkReachabilityManager *reachability;

// LOGGING
@property (assign, nonatomic) BOOL debugPostRequests;
@property (assign, nonatomic) BOOL debugPostResponses;

// FIELDS
@property (strong, nonatomic) NSString *apiUrl;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSNumber *userid;
@property (strong, nonatomic) User *user;

- (void) setupManager;

/**
 * verifica se c'è la possibilità di fare una auto-autenticazione (login effettuata precedentemente)
 */
- (BOOL) canAuthenticate;

/**
 * verifica se il token è presente in facade, pertanto la login è stata effettuata
 */
- (BOOL) isAuthenticated;

/**
 * verifica se c'è un profilo utente in facade e se questo non possiede email e numero di telefono (ghost)
 */
- (BOOL) isGhost;

/**
 *  Check for internet connection
 *
 *  @return
 */
- (ResponseObject*) checkForInternetConnection;

/**
 *  Generic REST API call
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param callback
 */
- (void) restCall:(NSString*)url method:(NSString*)method params:(NSDictionary*)params callback:(ResponseObjectCallback)callback;

/**
 *  Generic String API call
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param callback
 */
- (void) stringCall:(NSString*)url method:(NSString*)method params:(NSDictionary*)params callback:(ResourceCallback)callback;

@end
