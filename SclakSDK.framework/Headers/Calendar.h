//
//  Calendar.h
//  SclakApp
//
//  Created by albi on 05/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Day.h"
#import "Month.h"
#import "Year.h"
#import "Weekday.h"
#import "Period.h"
#import "TimeRange.h"

@interface Calendar : JSONModel

@property (nonatomic, strong) NSMutableOrderedSet<Day> *days;
@property (nonatomic, strong) NSMutableOrderedSet<Month> *months;
@property (nonatomic, strong) NSMutableOrderedSet<Year> *years;
@property (nonatomic, strong) NSMutableOrderedSet<Weekday, Optional> *weekdays;
@property (nonatomic, strong) NSMutableOrderedSet<Period, Optional> *periods;

- (BOOL) isEmpty;

/**
 * check if the calendar has any period. if so, the UI will allow to input only periods.
 * otherwise, the UI will allow to input days, months and years
 */
- (BOOL) hasPeriods;

- (void) addOrReplaceYear:(Year*)year;
- (void) addOrReplaceMonth:(Month*)month;
- (void) addOrReplaceDay:(Day*)day;
- (void) addOrReplaceWeekday:(Weekday*)weekday;
- (void) addOrReplacePeriod:(Period*)period;

- (TimeRange*) getTimeLimitForDay:(Day*)day;
- (TimeRange*) getTimeLimitForMonth:(Month*)month;
- (TimeRange*) getTimeLimitForYear:(Year*)year;
- (TimeRange*) getTimeLimitForWeekday:(Weekday*)weekday;
- (TimeRange*) getTimeLimitForPeriod:(Period*)period;

- (CGFloat) getSettedPercentageForMonth:(Month*)month;
- (CGFloat) getSettedPercentageForYear:(Year*)year;
- (CGFloat) getSettedPercentageForWeekday:(Weekday*)weekday;
- (CGFloat) getSettedPercentageForPeriod:(Period*)period;

- (void) setTimeRange:(TimeRange*)timeRange forDays:(NSArray*)days;
- (void) setTimeRange:(TimeRange*)timeRange forMonths:(NSArray*)months;
- (void) setTimeRange:(TimeRange*)timeRange forYears:(NSArray*)years;
- (void) setTimeRange:(TimeRange*)timeRange forWeekdays:(NSArray*)weekdays;
- (void) setTimeRange:(TimeRange*)timeRange forPeriods:(NSArray*)periods;

- (void) clearTimeRangeForDays:(NSArray*)days;
- (void) clearTimeRangeForMonths:(NSArray*)months;
- (void) clearTimeRangeForYears:(NSArray*)years;
- (void) clearTimeRangeForWeekdays:(NSArray*)weekdays;
- (void) clearTimeRangeForPeriods:(NSArray*)periods;

- (NSOrderedSet*)getAllDaysForMonth:(Month*)month;
- (NSOrderedSet*)getAllDaysForYear:(Year*)year;
- (NSOrderedSet*)getMonthsForYear:(Year*)year;

- (void) normalizeDaysInMonth:(Month*)month;
- (void) normalizeMonthsInYear:(Year*)year;

- (void) denormalizeMonth:(Month*)month;
- (void) denormalizeYear:(Year*)year;

- (BOOL) existDay:(Day*)day;
- (BOOL) existMonth:(Month*)month;
- (BOOL) existYear:(Year*)year;
- (BOOL) existWeekday:(Weekday*)weekday;
- (BOOL) existPeriod:(Period*)period;
- (Period*) periodWithDay:(Day*)day;

- (void) removeDay:(Day*)day;
- (void) removeMonth:(Month*)month;
- (void) removeYear:(Year*)year;
- (void) removeWeekday:(Weekday*)weekday;
- (void) removePeriod:(Period*)period;

- (void) clearAllDaysForMonth:(Month*)month;
- (void) clearAllMonthsForYear:(Year*)year;

- (BOOL) haveSelectedDaysSameTimeRange:(NSArray*)selectedDays;
- (BOOL) haveSelectedMonthsSameTimeRange:(NSArray*)selectedMonths;
- (BOOL) haveSelectedYearsSameTimeRange:(NSArray*)selectedYears;
- (BOOL) haveSelectedWeekdaysSameTimeRange:(NSArray*)selectedWeekdays;
- (BOOL) haveSelectedPeriodsSameTimeRange:(NSArray*)selectedPeriods;

@end
