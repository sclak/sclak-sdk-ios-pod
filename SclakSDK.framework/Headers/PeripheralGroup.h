//
//  PeripheralGroup.h
//  SclakFacade
//
//  Created by Daniele Poggi on 27/03/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "PeripheralBaseModel.h"
#import "PeripheralProtocols.h"
#import "SCKFacadeCallbacks.h"

@interface PeripheralGroups : ResponseObject

@property (nonatomic, strong) NSArray<PeripheralGroup, Optional> *list;

@end

@interface PeripheralGroup : PeripheralBaseModel

@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *tag;

@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSNumber<Optional> *shared;

@property (nonatomic, strong) NSArray<Peripheral, Optional> *peripherals;

@property (nonatomic, strong) NSNumber<Optional> *page;
@property (nonatomic, strong) NSNumber<Optional> *pageSize;

@property (nonatomic, strong) NSNumber<Optional> *canInviteOwner;
@property (nonatomic, strong) NSNumber<Optional> *canInviteGuest;

- (BOOL) ownerPostPrivilegeEnabled;
- (BOOL) isPinPukSupported;
- (BOOL) hasPairedKeyboard;

- (Peripheral*) peripheralWithLowestIdInGroup;

#pragma mark - Rest APIs

+ (void) all:(ResponseObjectCallback)callback;
+ (void) allWithOptions:(NSDictionary*)options callback:(ResponseObjectCallback)callback;
+ (void) one:(NSString*)idOrTag callback:(ResponseObjectCallback)callback;

@end
