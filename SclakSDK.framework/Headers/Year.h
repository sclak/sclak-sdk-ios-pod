//
//  Year.h
//  SclakApp
//
//  Created by albi on 05/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "TimeRange.h"

@protocol Year
@end

@interface Year : JSONModel

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, strong) TimeRange *timeRange;

@property (nonatomic, strong) NSIndexPath <Ignore> *indexPath;

- (BOOL) hasSameTimeRange:(Year*)year;

@end
