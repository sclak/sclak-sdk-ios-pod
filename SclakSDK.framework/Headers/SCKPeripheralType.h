//
//  PeripheralType.h
//  SclakFacade
//
//  Created by Daniele Poggi on 16/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface SCKPeripheralType : ResponseObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *fwCode;
@property (nonatomic, strong) NSString<Optional> *beaconUuid;
@property (nonatomic, strong) NSArray<Optional> *supportedAccessories;

- (BOOL) supportAccessoryWithTypeCode:(NSString*)type;

@end
