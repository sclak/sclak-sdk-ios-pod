//
//  PeripheralUser.h
//  Sclak2
//
//  Created by albi on 06/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "JSONModel.h"

#define kGroupTagInstaller  @"installer"
#define kGroupTagAdmin      @"admin"
#define kGroupTagOwner      @"owner"
#define kGroupTagGuest      @"guest"

@protocol PeripheralUser
@end

@interface PeripheralUser : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *surname;
@property (nonatomic, strong) NSArray<Optional> *actions;
@property (nonatomic, strong) NSArray<Optional> *groups;
@property (nonatomic, strong) NSArray<Optional> *groupsTags;

+ (NSArray*) debugUsers;

- (BOOL) isGuest;
- (BOOL) isOwner;
- (BOOL) isAdmin;

@end
