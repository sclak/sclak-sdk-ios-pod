//
//  AccessCode.h
//  SclakApp
//
//  Created by albi on 22/06/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "ResponseObject.h"

@protocol PinCode
@end

@interface PinCodes : ResponseObject

@property (nonatomic, strong) NSArray <Optional, PinCode> *list;

@end

@interface PinCode : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *code;
@property (nonatomic, strong) NSString <Optional> *clearCode;
@property (nonatomic, strong) NSNumber <Optional> *peripheralId;
@property (nonatomic, strong) NSString <Optional> *btcode;
@property (nonatomic, strong) NSNumber <Optional> *fwPosition;
@property (nonatomic, strong) NSNumber <Optional> *fwUpdated;
@property (nonatomic, strong) NSNumber <Optional> *fwDelete;
@property (nonatomic, strong) NSNumber <Optional> *editTime;
@property (nonatomic, strong) NSString <Optional> *command;
@property (nonatomic, strong) NSString <Optional> *flags;

@end
