//
//  Token.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "User.h"

@interface AuthToken : ResponseObject

@property (nonatomic, strong) NSString<Optional> *token;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) User<Optional> *user;
@property (nonatomic, strong) NSNumber<Optional> *expiryDate;
@property (nonatomic, strong) NSNumber<Optional> *creationDate;
@property (nonatomic, strong) NSNumber<Optional> *tokenDuration;

+ (void) getAuthTokenCallback:(ResourceCallback)callback;
+ (void) getAuthTokenWithUsername:(NSString*)username password:(NSString*)password callback:(ResourceCallback)callback;

@end
