//
//  LogUsage.h
//  SclakFacade
//
//  Created by Daniele Poggi on 23/08/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SCKFacadeCallbacks.h"

@interface LogUsage : JSONModel

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber *logTime;

#pragma mark - Log Usages Access Control APIs

+ (void) logUsages:(NSArray*)usages disableNotification:(BOOL)disableNotification callback:(ResourceCallback)callback;
+ (void) logUsageForBtCode:(NSString*)btcode callback:(ResourceCallback)callback;
+ (void) logUsageForBtCode:(NSString*)btcode anonymous:(BOOL)anonymous logTime:(NSNumber*)logTime disableNotification:(BOOL)disableNotification callback:(ResourceCallback)callback;
+ (void) getLogUsageForBtcode:(NSString*)btcode page:(NSInteger)page userID:(NSNumber*)userID callback:(ResourceCallback)callback;

@end