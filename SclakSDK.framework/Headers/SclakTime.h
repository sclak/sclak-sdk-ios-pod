//
//  Time.h
//  SclakFacade
//
//  Created by albi on 12/11/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface SclakTime : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *erroCode;
@property (nonatomic, strong) NSString <Optional> *ipaddress;
@property (nonatomic, strong) NSString <Optional> *timezone;
@property (nonatomic, strong) NSNumber <Optional> *time;

@end
