//
//  SCKFacadeCallbacks.h
//  SclakFacade
//
//  Created by Daniele Poggi on 27/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"

// CALLBACKS
typedef void(^ResourceCallback)(BOOL success, id result);
typedef void(^ResponseObjectCallback)(BOOL success, id result, ResponseObject *responseObject);
typedef void(^LoginErrorCallback)(BOOL success, NSNumber *errorCode);
