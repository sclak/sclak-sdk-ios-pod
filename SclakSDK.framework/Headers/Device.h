//
//  Device.h
//  Sclak2
//
//  Created by albi on 04/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@protocol Device
@end

@interface Devices : ResponseObject

@property (nonatomic, strong) NSArray<Device, Optional>*list;

@end

@interface Device : ResponseObject

@property (nonatomic, copy) NSNumber <Optional> *id;
@property (nonatomic, copy) NSString <Optional> *uniqueId;
@property (nonatomic, copy) NSString <Optional> *brand;
@property (nonatomic, copy) NSString <Optional> *model;
@property (nonatomic, copy) NSString <Optional> *os;
@property (nonatomic, copy) NSString <Optional> *osVersion;
@property (nonatomic, copy) NSString <Optional> *name;
@property (nonatomic, copy) NSNumber <Optional> *userDeviceId;
@property (nonatomic, strong) NSNumber <Optional> *enabled;
@property (nonatomic, strong) NSNumber <Optional> *signedOut;
@property (nonatomic, strong) NSNumber <Optional> *lastGetTokenTime;
@property (nonatomic, strong) NSNumber <Optional> *lastUsageTime;

- (NSDate*)getLastTokenTime;
- (NSDate*)getLastUsageTime;

#pragma mark - Devices API

+ (void) logoutWithUserDeviceCallback:(ResourceCallback)callback;
+ (void) deleteUserDeviceCallback:(ResourceCallback)callback;

@end
