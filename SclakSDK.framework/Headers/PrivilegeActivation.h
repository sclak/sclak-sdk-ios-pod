//
//  Activation.h
//  SclakFacade
//
//  Created by Daniele Poggi on 18/09/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PrivilegeActivation : ResponseObject

// single peripheral activation
@property (nonatomic, strong) NSNumber <Optional> *peripheralId;
@property (nonatomic, strong) NSString <Optional> *btcode;

// peripheral group activation
@property (nonatomic, strong) NSString <Optional> *peripheralGroupTag;

// user group tag, summarize what kind of privilege has been activated
@property (nonatomic, strong) NSString <Optional> *groupTag;

@end
