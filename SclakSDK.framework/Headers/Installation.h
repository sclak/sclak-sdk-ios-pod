//
//  Installation.h
//  SclakFacade
//
//  Created by Daniele Poggi on 17/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "User.h"

@interface Installation : ResponseObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) User<Optional> *installer;
@property (nonatomic, strong) User<Optional> *admin;

@property (nonatomic, strong) NSNumber<Optional> *installTime;
@property (nonatomic, strong) NSNumber<Optional> *authorizationTime;
@property (nonatomic, strong) NSNumber<Optional> *acknowledgeTime;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *editTime;

@property (nonatomic, strong) NSArray<Optional> *peripherals;

@end