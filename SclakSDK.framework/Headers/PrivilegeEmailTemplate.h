//
//  PrivilegeEmailTemplate.h
//  SclakFacade
//
//  Created by albi on 15/07/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PrivilegeEmailTemplate : ResponseObject

@property (nonatomic, strong) NSString *template;

@end
