//
//  Month.h
//  SclakApp
//
//  Created by albi on 05/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "TimeRange.h"

@protocol Month
@end

@interface Month : JSONModel

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger year;
@property (nonatomic, strong) TimeRange *timeRange;

@property (nonatomic, strong) NSIndexPath <Ignore> *indexPath;

- (BOOL) hasSameYearAndTimeRange:(Month*)object;
- (BOOL) hasSameTimeRange:(Month*)month;

@end
