//
//  Constraint.h
//  SclakApp
//
//  Created by albi on 17/12/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "JSONModel.h"

@protocol Constraint
@end

@interface Constraint : JSONModel

@property (nonatomic, strong) NSString <Optional> *field;
@property (nonatomic, strong) NSArray <Optional> *values;

@end
