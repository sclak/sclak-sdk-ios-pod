//
//  Common.h
//  Facade
//
//  Created by albi on 29/06/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "JSONModel.h"
#import "NSDate+Helper.h"
#import "AFNetworking.h"
#import "NSData+SCKStringGenerator.h"
#import "NSString+SCKDataGenerator.h"
#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0];

#define UIControlStateAll UIControlStateNormal & UIControlStateSelected & UIControlStateHighlighted

#define MIN_PASSWORD_LENGTH 8

#define NSLogIillegalVariable(variable) NSLog((@"%s ILLEGAL ARGUMENT for variable: %s is %@"), __PRETTY_FUNCTION__, #variable, variable)

// FILES
#define PROFILE_FILE                            @"profile.json"
#define PERIPHERALS_FILE                        @"peripherals.json"
#define USERS_FILE                              @"users.json"
#define USER_DEVICES_FILE                       @"user_devices.json"
#define FIRMWARE_FILE                           @"firmware.json"
#define PIN_MANAGER_FILE                        @"pin_manager.json"
#define PERIPHERAL_GROUPS_FILE                  @"peripheral_groups.json"

typedef NS_ENUM(NSUInteger, ApiConfig) {
    ApiConfigProduction,
    ApiConfigDevelopment,
    ApiConfigIntegration
};

typedef NS_ENUM(NSUInteger, UserStatus) {
    UserStatusDisabled = 0,
    UserStatusPending = 1,
    UserStatusActive = 2,
};

typedef NS_ENUM(NSInteger, SCKUserStatus) {
    SCKUserGuest = -1,
    SCKUserAdmin = 0,
    SCKUserDisabled = 1,
    SCKUserOwner = 2,
    SCKUserInstaller = 3,
};

typedef NS_ENUM(NSUInteger, PeripheralStatus) {
    PeripheralStatusAdded = 0,      // significa presente su database ma non prodotto hw
    PeripheralStatusUsed = 3,       // significa hw prodotto e inscatolato, contettualmente "in vendita"
    PeripheralStatusActive = 6,     // significa hw installato pronto da testare
    PeripheralStatusTested = 9      // significa hw installato e testato, pronto all'uso
};

typedef NS_ENUM(NSInteger, SCKLockStatus) {
    SCKLockStatusUnavailable = -1,
    SCKLockStatusDisabled = 0,
    SCKLockStatusEnabled = 1
};

typedef NS_ENUM(NSUInteger, ChangePasswordControllerType) {
    ChangePassword = 1,
    PasswordForgot = 2
};

typedef NS_ENUM(NSUInteger, ActionREST) {
    CREATE,
    RETRIEVE,
    UPDATE,
    DELETE
};

typedef NS_ENUM (NSUInteger, PrivilegeAction) {
    CONFIRM,
    DENY
};

typedef NS_ENUM(NSInteger, SclakType) {
    SclakTypeDoor,
    SclakTypeGate,
    SclakTypeGarage
};

