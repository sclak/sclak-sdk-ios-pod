//
//  Privilege.h
//  SclakApp
//
//  Created by Haiku Oezu on 20/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Action.h"
#import "Calendar.h"
#import "Coupon.h"
#import "Sender.h"
#import "PrivilegeOption.h"
#import "SCKFacadeCallbacks.h"

#define PrivilegeStatusPendingReset      3
#define PrivilegeStatusActive            2
#define PrivilegeStatusPending           1
#define PrivilegeStatusDisabled          0
#define PrivilegeStatusDeleted           -1

#define kSecurityLevelLow                0
#define kSecurityLevelMedium             1
#define kSecurityLevelHigh               2

@protocol Privilege

@end

@interface Privileges : ResponseObject

@property (nonatomic, strong) NSArray<Privilege> *list;

@end

@class Peripheral;
@class PeripheralGroup;

@interface Privilege : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSString<Optional> *targetType;
@property (nonatomic, strong) NSString<Optional> *targetField;
@property (nonatomic, strong) NSString<Optional> *targetValue;
@property (nonatomic, strong) NSString<Optional> *groupTag;
@property (nonatomic, strong) NSString<Optional> *userEmail;
@property (nonatomic, strong) NSString<Optional> *userName;
@property (nonatomic, strong) NSString<Optional> *userSurname;
@property (nonatomic, strong) NSString<Optional> *userPhoneNumber;
@property (nonatomic, strong) NSNumber<Optional> *couponId;
@property (nonatomic, strong) NSNumber<Optional> *oneTime;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) Calendar<Optional> *timeConstraints;
@property (nonatomic, strong) Coupon<Optional> *coupon;
@property (nonatomic, strong) NSArray<Action, Optional> *actions;
@property (nonatomic, strong) NSNumber<Optional> *confirmRequired;
@property (nonatomic, strong) NSNumber<Optional> *expireTime;
@property (nonatomic, strong) NSNumber<Optional> *modifiedTime;
@property (nonatomic, strong) NSNumber<Optional> *lastUsageTime;
@property (nonatomic, strong) NSNumber<Optional> *pinCodeId;
@property (nonatomic, strong) NSString<Optional> *pinCode;
@property (nonatomic, strong) NSString <Optional> *pukCode;

// invite
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *inviteExpireTime;

// sender
@property (nonatomic, strong) NSNumber <Optional> *senderId;
@property (nonatomic, strong) Sender <Optional> *sender;

// peripheral accessory
@property (nonatomic, strong) NSString <Optional> *peripheralName;
@property (nonatomic, strong) NSString <Optional> *peripheralDesc;

// last request time
@property (nonatomic, strong) NSNumber<Optional> *lastRequestTime;

// privilege option
@property (nonatomic, strong) PrivilegeOption<Optional> *privilegeOption;

// privilege "check_only" flag, used in Privilege POST
@property (nonatomic, strong) NSNumber<Optional> *checkOnly;

/**
 * retrieve own cached privileges with specific group tag
 */
+ (NSArray<Privilege>*) cachedPrivilegesWithGroupTag:(NSString*)groupTag;

/**
 * get the peripheral target of this privilege if available in Facade cache
 * return nil otherwise
 */
- (Peripheral*) peripheral;

/**
 * get the peripheral group target of this privilege if available in Facade cache
 * return nil otherwise
 */
- (PeripheralGroup*) peripheralGroup;

/**
 * checks that both target field and target value are setted
 */
- (BOOL) isValid;

- (BOOL) isPrivilegeTypePeripheral;
- (BOOL) isPrivilegeTypePeripheralGroup;

- (BOOL) isPrivilegeGuest;
- (BOOL) isPrivilegeOwner;
- (BOOL) isPrivilegeAdmin;
- (BOOL) isPrivilegeInstaller;

- (BOOL) isActive; // 2 active
- (BOOL) isPending;
- (BOOL) isDisabled;
- (BOOL) isPendingReset;

- (BOOL) inviteExpired;

- (NSString*)fullName;

- (BOOL) isEnabledForDate:(NSDate*)date withPeripheral:(Peripheral*)peripheral;

- (NSDate*) lastRequestDate;

- (void) setActive;
- (void) setPending;
- (void) setDisabled;
- (void) setPendingReset;

#pragma mark - Privileges API

+ (void) getSentPrivilegesCallback:(ResourceCallback)callback;

@end
